import NativeModule from "../native";

export interface HelloWorld {
    hello(): string;
}

export default NativeModule as HelloWorld;
