import "mocha";
import expect from "expect";

import NativeModule from "../src";

describe("hello(): string", () => {

    it("returns correct string", () => {
        const result = NativeModule.hello();
        expect(result).toEqual("Hello, World!");
    });

});