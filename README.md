# neon-typed

Build native [Node.js](https://nodejs.org/en/) modules in [Rust](https://www.rust-lang.org/) (powered by [Neon](https://neon-bindings.com/)).

Declare [Typescript](https://www.typescriptlang.org/) types and test with [Mocha](https://mochajs.org/).

# NPM Scripts

`> npm test`

Run Mocha tests in `./test` folder

`> npm run build`

Build release Rust module and Typescript files (including declarations).
